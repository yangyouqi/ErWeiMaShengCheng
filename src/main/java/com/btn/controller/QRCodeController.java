package com.btn.controller;


import com.btn.utils.QrCodeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

@RestController
@RequestMapping(value = "/api")
@Api(value = "二维码接口", tags = "生成二维码接口", description = "")
public class QRCodeController {

    /**
     * 二维码
     * @param request
     * @param response
     */
    @ApiOperation(value = "二维码测试接口", notes = "二维码接口")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful request"),
            @ApiResponse(code = 500, message = "internal server error")})
    @RequestMapping(value = "/v1/qrcode", method = RequestMethod.GET)
    public void qrcode(HttpServletRequest request, HttpServletResponse response) {
//        StringBuffer url = request.getRequestURL();
        // 域名
//        String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();

        // 再加上请求链接
        String requestUrl = "http://www.yangyoki.top";
        try {
            OutputStream os = response.getOutputStream();
            QrCodeUtils.encode(requestUrl, "/static/images/logo.png", os, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
